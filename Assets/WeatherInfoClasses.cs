﻿using System;
using System.Collections.Generic;

[Serializable]
public class WeatherInfo
{
    public List<WeatherCityItem> list;
}

[Serializable]
public class WeatherCityItem
{
    public string name;
    public MainWeatherInfo main;
    public List<WeatherItem> weather;
}

[Serializable]
public class MainWeatherInfo
{
    public float temp;
}

[Serializable]
public class WeatherItem
{
    public string main;
}
