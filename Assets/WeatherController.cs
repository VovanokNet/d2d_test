﻿using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    private const string URL_FORMAT = "http://api.openweathermap.org/data/2.5/find?lat=58.02&lon=56.3&cnt={0}&appid=51805bdd8e9120e6e0ab93ad818af091";
    private const string LAST_WEATHER_KEY = "lastWeatherState";

    private ApplicationController appController;
    private ActivityMeter activityMeter;
    private WeatherInfo currentWeatherInfo;

    private void Start()
    {
        appController = FindObjectOfType<ApplicationController>();
        appController.OnAppLostFocus += ApplicationLostFocus;

        activityMeter = FindObjectOfType<ActivityMeter>();

        currentWeatherInfo = JsonUtility.FromJson<WeatherInfo>(PlayerPrefs.GetString(LAST_WEATHER_KEY));
    }

    private void ApplicationLostFocus()
    {
        StartCoroutine("GetWeatherInfo");
    }

    private IEnumerator GetWeatherInfo()
    {
        var www = new WWW(string.Format(URL_FORMAT, (int)Mathf.Clamp(activityMeter.ActivityTimeSec, 1, 50)));
        yield return www;

        PlayerPrefs.SetString(LAST_WEATHER_KEY, www.text);

        currentWeatherInfo = JsonUtility.FromJson<WeatherInfo>(www.text);
    }

    StringBuilder sbGuiText = new StringBuilder();

    private void ClearGui()
    {
        sbGuiText.Length = 0;
        sbGuiText.Capacity = 0;

        sbGuiText.Append("Город".PadRight(50));
        sbGuiText.Append("Температура".PadRight(50));
        sbGuiText.Append("Тип".PadRight(50));
    }

    private void OnGUI()
    {
        if (currentWeatherInfo != null && currentWeatherInfo.list != null)
        {
            ClearGui();

            foreach (var cityItem in currentWeatherInfo.list)
            {
                var weather = cityItem.weather.FirstOrDefault();

                sbGuiText.Append(cityItem.name.PadRight(50));
                sbGuiText.Append(cityItem.main.temp.ToString().PadRight(50));
                sbGuiText.Append((weather != null ? weather.main : string.Empty).PadRight(50));
                sbGuiText.AppendLine();
            }

            GUI.Label(new Rect(100, 100, 600, 100), sbGuiText.ToString());
        }
    }
}
