﻿using UnityEngine;

public class ActivityMeter : MonoBehaviour
{
    public float ActivityTimeSec { get; private set; }

    private ApplicationController appController;
    private float lastStartActivityTimeMs;

	private void Start()
    {
        appController = FindObjectOfType<ApplicationController>();
        if (appController == null)
        {
            Debug.LogError("Application controller is't found");
            return;
        }

        lastStartActivityTimeMs = Time.time;

        appController.OnAppLostFocus += ApplicationLostFocusHandler;
    }

    private void OnDestroy()
    {
        if (appController != null)
        {
            appController.OnAppLostFocus -= ApplicationLostFocusHandler;
        }
    }

    private void ApplicationLostFocusHandler()
    {
        ActivityTimeSec += Time.time - lastStartActivityTimeMs;
        lastStartActivityTimeMs = Time.time;
    }
}
