﻿using System;
using UnityEngine;

public class ApplicationController : MonoBehaviour
{
    public event Action OnAppGetFocus;
    public event Action OnAppLostFocus;

    private void Start()
    {
        if (OnAppGetFocus != null)
            OnAppGetFocus();
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            if (OnAppGetFocus != null)
                OnAppGetFocus();
        }
        else
        {
            if (OnAppLostFocus != null)
                OnAppLostFocus();
        }
    }

    private void OnApplicationQuit()
    {
        if (OnAppLostFocus != null)
            OnAppLostFocus();
    }
}
